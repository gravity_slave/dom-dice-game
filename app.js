/**
 * Created by timur on 28.01.17.
 */
var scores, roundscore, activePlayer, gameStatus ;

init();


function init() {
    scores = [0,0];
    roundscore = 0;
    activePlayer = 0;
    gameStatus = true;
    document.querySelector('.dice').style.display ='none';
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.getElementById('name-1').textContent = 'Player 2';
    document.getElementById('name-0').textContent = 'Player 1';

    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('winner');

    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');


}

var lastDice;

document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gameStatus) {



        var dice = Math.floor(Math.random() * 6) + 1;
        var diceDom = document.querySelector('.dice');
        diceDom.style.display = 'block';
        diceDom.src = 'dice-' + dice + '.png';
        // if it's two sixes in a row then nulify  total score
        if (dice === 6 && lastDice === 6) {
            scores[activePlayer] = 0;
            document.querySelector('#score-' +activePlayer).textContent = '0';
            document.querySelector('#score-' + activePlayer).textContent = '0';
            nextPlayer();

        } else  if (dice !== 1) {
            roundscore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundscore;

        } else {
            lastDice = -1;
            nextPlayer();

        }
        lastDice = dice;
    }
});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gameStatus) {
        scores[activePlayer] += roundscore;
        document.getElementById('score-' + activePlayer).textContent  = scores[activePlayer]
        var input = document.querySelector('.final-score').value;
        var winningScore
        // undefined, null, 0 or "" coerced to false
        if (input) {
             winningScore = input;
        } else {
            winningScore =  100;
        }
        if (scores[activePlayer] >= winningScore) {
            document.querySelector('#name-' + activePlayer).textContent = 'Teh Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gameStatus = false;
        }else {
            nextPlayer();
        }

    }

});

function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundscore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');


}

document.querySelector('.btn-new').addEventListener('click', init);